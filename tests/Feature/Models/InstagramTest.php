<?php

namespace Tests\Feature\Models;

use App\Enums\InstagramMediaType;
use App\Enums\ProducerType;
use App\Models\Instagram;
use App\Models\InstagramMedias;
use App\Models\Producer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class InstagramTest extends TestCase
{
    use RefreshDatabase;
    /**
     * assertDatabaseCount
     *
     * Assert that a table in the database contains the given number of records
     */
    public function test_stored_data(): void
    {
        Instagram::factory()->create();
        $this->assertDatabaseCount(Instagram::class , 1);
    }

    /*
     * assertDatabaseHas
     *
     * Assert that a table in the database contains records matching the given key
     */
    public function test_stored_data_is_the_same_data_that_we_gave_it(): void
    {
        $data = Instagram::factory()->make()->toArray();
        Instagram::create($data);

        $this->assertDatabaseHas(Instagram::class , [
            "producer_id" => $data["producer_id"],
            "title" => $data["title"],
            "content"=> $data["content"],
            "source_link"=> $data["source_link"],
            "publication_date"=> $data["publication_date"],
        ]);
    }

    /*
     * BelongsTo
     *
     * this test check BelongsTo relation 
     */
    public function test_instagram_relationship_with_producer()
    {
        $instagram = Instagram::factory()->for(Producer::factory()->create([
            "type" => ProducerType::Instagram
        ]))->create();
        $this->assertTrue(isset($instagram->producer->id));
        $this->assertTrue($instagram->producer instanceof Producer);
    }


    public function test_instagram_relationship_with_videos()
    {
        $count = rand(1,10);

        $instagram = Instagram::factory()->has(InstagramMedias::factory()->count($count)->set("type" , InstagramMediaType::Video->value),'videos')->create();

        $this->assertCount($count,$instagram->videos);
        $this->assertTrue($instagram->videos()->first() instanceof InstagramMedias);
    }


}
